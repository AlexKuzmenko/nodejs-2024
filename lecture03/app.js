const mongoose = require('mongoose');
require('dotenv').config();
let db_password = process.env.DB_PASS;
let db_user = process.env.MONGO_USER;
mongoose.connect(`mongodb+srv://${db_user}:${db_password}@cluster0.qyuugzt.mongodb.net/taskApp?retryWrites=true&w=majority&appName=Cluster0`);

const User = mongoose.model('User', {
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    }
});

const user1 = new User({
    firstName: "John",
    lastName: "Smith"
});

user1.save()
    .then((result) => console.log(result))
    .catch((error) => console.log(error.message));