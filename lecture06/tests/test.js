const {isSimple, factorial} = require('./../src/functions')
var assert = require('assert');

//Набір тестових варіантів функції isSimple(n)
const testData = [
    {n: 1, result: false},
    {n: -1, result: false},
    {n: -2, result: false},
    {n: -3, result: false},
    {n: -4, result: false},
    {n: -10, result: false},
    {n: 2, result: true},
    {n: 3, result: true},
    {n: 4, result: false},
    {n: 5, result: true},
    {n: 7, result: true},
    {n: 8, result: false},
    {n: 13, result: true},
    {n: 15, result: false},
    {n: 19, result: true},
    {n: 51, result: false},
    {n: 53, result: true},
];

const testDataFactorial = [
    {n: 0, result: 1},
    {n: 1, result: 1},
    {n: 2, result: 2},
    {n: 3, result: 6},
    {n: 4, result: 24},
    {n: 5, result: 120},
    {n: -1, result: "Undefined"},
    {n: -2, result: "Undefined"},
]

describe("Functions", function() {
    describe('#isSimple()', function () {
        for (let i = 0; i < testData.length; i++) {
            it(`should return ${testData[i].result} when the value is ${testData[i].n}`, function () {
                assert.equal(isSimple(testData[i].n), testData[i].result);
            });
        }
    });

    describe('#Factorial()', function () {
        for (let i = 0; i < testDataFactorial.length; i++) {
            it(`should return ${testDataFactorial[i].result} when the value is ${testDataFactorial[i].n}`, function () {
                assert.equal(factorial(testDataFactorial[i].n), testDataFactorial[i].result);
            });
        }
    });
})


