
const isSimple = (n) => {
    if (n <= 1) return false;
    let top = Math.round(Math.sqrt(n));
    for (let i = 2; i <= top; i++) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

const factorial = (n) => {
    if (n < 0) return "Undefined";
    if (n == 0) return 1;
    return n * factorial(n - 1);
}

module.exports = {isSimple, factorial}