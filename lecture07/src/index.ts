type Customer = {
    name: string,
    email: string,
}

let user: Customer = {
    name: "Oleksandr",
    email: "alexkuzmenko2000@gmail.com"
}

console.log(user?.birthday)
