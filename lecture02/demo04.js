const cities = [
    {id: 1, name: "Zhytomyr"},
    {id: 2, name: "Kyiv"},
    {id: 3, name: "Vinnytsya"},
]

const coords = [
    {id: 1, coords: {lat: 50, long: 49}},
    {id: 2, coords: {lat: 49, long: 50}},
]

const getId = (city) => {
    return new Promise(((resolve, reject) => {
        setTimeout(() => {
            const filtered = cities.filter(item => item.name == city)
            if (filtered[0]) {
                resolve(filtered[0].id)
            }
            reject(new Error("City Not Found"))
        }, 2000)
    }))
}

const getCoords = (id) => {
    return new Promise(((resolve, reject) => {
        setTimeout(() => {
            const filtered = coords.filter(item => item.id == id)
            if (filtered[0]) {
                resolve(filtered[0].coords)
            }
            reject(new Error("Location Not Found"))
        }, 2000)
    }))
}

// getId("Odesa").then((id) => {
//     console.log(id)
//     return getCoords(id)
// }).then((location) => {
//     console.log(location)
// }).catch((error) => {
//     console.log(error.message)
// })

const doQuery = async (city) => {
    let id = await getId(city);
    console.log(id);
    let location = await getCoords(id);
    return location
}

doQuery("Kyiv").then((result) => {
    console.log(result)
}).catch((error) => {
    console.log(error.message)
});