function sum(a, b, callback) {
    setTimeout(function() {
        let c = a + b;
        callback(c);
    }, 2000)
}

sum(3, 4, function(result) {
    console.log(result);
})