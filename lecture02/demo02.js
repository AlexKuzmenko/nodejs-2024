const cities = [
    {id: 1, name: "Zhytomyr"},
    {id: 2, name: "Kyiv"},
    {id: 3, name: "Vinnytsya"},
]

const getId = (city, callback) => {
    setTimeout(() => {
        let cityItem = cities.find(item => item.name == city)
        if (cityItem) {
            callback(undefined, cityItem.id)
            return;
        }
        callback("City not found", undefined)
    }, 2000)
}

getId("Zhytomyr", (error, id) => {
    if (error) {
        return console.log(error)
    }
    console.log(id)
})