const myArray: Readonly<Array<number>> = [1, 2, 3, 4];
myArray.push(5);

interface Car {
    model: string,
    year: number
}

const myCar: Readonly<Car> = {
    model: "BMW",
    year: 2000
}

myCar.model = "Opel"